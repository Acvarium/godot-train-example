extends Button
var id = -1
var train_control = null

func setup(_id, _train_control):
	id = _id 
	train_control = _train_control
	text = "Switch " + str(id) 

func _on_SwitchButton_pressed():
	if id >= 0 and train_control:
		train_control.set_switch_value(id, self.pressed)
	
