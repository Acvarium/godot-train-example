extends Spatial
var cars = []														# масив, що містить посилання на об'єкти вагонів
var point_obj = preload("res://pointt.tscn")
var pointt_green_obj = preload("res://pointt_green.tscn")
var pointt_named_obj = preload("res://pointt_named.tscn")
var pointt_another_obj = preload("res://pointt_another.tscn")
var switche_button_obj = preload("res://objects/UI/SwitchButton.tscn")


var car_distance = 0.6												# відстань між центрами вагонів
var path_switches = []												# булевий масив, що вказує, які з стрілок задіяно
onready var head = $Path/HeadPathFollow								# посилання на об'єкт голови
onready var root_path = $PathLines/PathRoot/Path					# посилання на кореневий шлях
var paths = []														# масив, що містить посилання на шляхи
var path_struct = []												# кожен елемент масиву є масивом з 2 чисел де:
																		# 1 адреса входу на лінію
																		# 2 адреса виходу з лінії
var speed = 1														# швидкість руху потяга
onready var train_head_offset = head.offset
var current_train_sector = [0,0]

var maximum_segments = 100

#                                       
# 88888  8888  88888  88888  8    8     
# 8   8  8     8   8  8   8  8    8     
# 8888   8888  88888  88  8  888888     
# 88  8  88    88  8  88  8    88       
# 88   8 8888  88  8  88888    88       
#                                       

func _ready():
#	Формування списку всіх вагонів
	for c in $Path.get_children():
		if c.name != "HeadPathFollow":
			cars.append(c)
	for p in $PathLines.get_children():
		if p.name != "PathRoot":
			paths.append(p.get_node("Path"))
	build_path()
	build_path_struct()


func _physics_process(delta):
#	if to_switch_path:
#		if path_can_be_switched():
#			switch_path()
	train_head_offset += speed * delta
	head.offset = train_head_offset
	for i in range(cars.size()):
		cars[i].offset = train_head_offset - car_distance * (i + 1) 
	update_train_sector_id()


#func position_to_point_addr(_pos):
#	var _paths = get_full_paths_list()
#	var closest = [0, 0]
#	var closest_dist = INF
#	for i in range(_paths.size()):
#		for j in range(_paths[i].curve.get_point_count()):
#			var current_point = _paths[i].curve.get_point_position(j) + \
#				_paths[i].global_transform.origin
#			var current_dist = _pos.distance_to(current_point)
#			if current_dist < closest_dist:
#				closest = [i,j]
#				closest_dist = current_dist
#	pass


#                                                                     
# 88888  8888  88888      88888  8888  8888  88888  88888  88888      
# 8   8  8       8        8   8  8     8  8    8    8  88  8   8      
# 88     8888    88       88888  8888  88      88   8   8  8888       
# 88 "8  88      88          88  88    88      88   8   8  88  8      
# 88888  8888    88       88888  8888  8888    88   88888  88   8     
#  

func get_current_train_sector():
	var sector_data = []
	if current_train_sector[1] > current_train_sector[0]:
		for i in range(current_train_sector[1], $Path.curve.get_point_count()):
			sector_data.append([\
				$Path.curve.get_point_position(i),\
				$Path.curve.get_point_in(i),\
				$Path.curve.get_point_out(i)])
		for i in range(0, current_train_sector[0] + 1):
			sector_data.append([\
				$Path.curve.get_point_position(i),\
				$Path.curve.get_point_in(i),\
				$Path.curve.get_point_out(i)])
	else:
		var start_range = current_train_sector[1]
		var end_range = current_train_sector[0] + 1
		for i in range(start_range, end_range):
			sector_data.append([\
				$Path.curve.get_point_position(i),\
				$Path.curve.get_point_in(i),\
				$Path.curve.get_point_out(i)])
	$VisSector.curve.clear_points()
	return sector_data
	

func update_train_sector_id():
	var ss = ""
	var _points = []
	var tail = cars[cars.size() - 1]
	for i in range(1, $Path.curve.get_point_count() - 1):
		_points.append($Path.curve.get_closest_offset(\
			$Path.curve.get_point_position(i)))
	_points.append($Path.curve.get_baked_length())
	ss += str("%.3f" % (head.offset))
	var head_sector = 0
	var tail_sector = 0
	for i in range(_points.size()):
		if  head.offset < _points[i]:
			head_sector = i + 1
			break
	for i in range(_points.size()):
		if tail.offset < _points[i]:
			tail_sector = i 
			break
	ss += "   " + str([head_sector, tail_sector])
	$Control/Label.text = ss
	if current_train_sector != [head_sector, tail_sector]:
		current_train_sector = [head_sector, tail_sector]
		var sector_data = get_current_train_sector()
		print("Update path... ")
		for sd in sector_data:
			$VisSector.curve.add_point(sd[0], sd[1], sd[2])
		show_paths()


func mark_path_points():
	var _paths = get_full_paths_list()
	var _rot = 0.0
	for p in _paths:
		for i in range(p.curve.get_point_count()):
			var _pos = p.curve.get_point_position(i) + p.global_transform.origin
			var point_inst = pointt_named_obj.instance()
			$Points.add_child(point_inst)
			point_inst.transform = point_inst.transform.rotated(Vector3(0,1,0), _rot)
			point_inst.global_transform.origin = _pos
			for k in range(i + 1):
				point_inst.get_node("index").get_child(k).visible = true
		_rot -= PI/4


func build_path_struct():
	path_struct.clear()
	path_switches.clear()
	for i in range(paths.size()):
		var nn = paths[i].name
		path_switches.append(false)
		var p = paths[i]
		if p.curve.get_point_count() < 2:
			path_struct.append([])
		else:
			var in_pos = p.curve.get_point_position(0) + \
				p.global_transform.origin
			var out_pos = p.curve.get_point_position(p.curve.get_point_count() - 1) + \
				p.global_transform.origin
			var in_data = find_closest_point(in_pos, i + 1)
			var out_data = find_closest_point(out_pos, i + 1)
			
			path_struct.append([in_data, out_data])
			var ss = str(in_data) + " " + str(out_data) + "\n"
#		var current_in = find_closest_point(p.)
	build_switches_ui()


func find_closest_point(_pos: Vector3, path_id):
	var _paths = get_full_paths_list()
	var closest = [0, 0]
	var closest_dist = INF
	for i in range(_paths.size()):
		if i == path_id:
			continue
		for j in range(_paths[i].curve.get_point_count()):
			var current_point = _paths[i].curve.get_point_position(j) + \
				_paths[i].global_transform.origin
			var current_dist = _pos.distance_to(current_point)
			if current_dist < closest_dist:
				closest = [i,j]
				closest_dist = current_dist
	return closest
	

func show_path(_path, _inst, step_len):
	for i in range(int(_path.curve.get_baked_length() / step_len)):
		var point_inst = _inst.instance()
		$Points.add_child(point_inst)
		point_inst.global_transform.origin = _path.curve.interpolate_baked(i * step_len) +\
			_path.global_transform.origin


func build_switches_ui():
	for b in $Control/Switches.get_children():
		b.queue_free()
	for i in range(path_switches.size()):
		var switch_button = switche_button_obj.instance()
		$Control/Switches.add_child(switch_button)
		switch_button.setup(i, self)

func show_paths():
	for p in $Points.get_children():
		p.free()
	var step_len = 0.2
	show_path(root_path, pointt_green_obj, step_len)
	for p in paths:
		show_path(p, pointt_green_obj, step_len)
	show_path($Path, point_obj, step_len)
	mark_path_points()
	show_path($VisSector, pointt_another_obj, step_len)


func point_exists(_point):
	var _paths = get_full_paths_list()
	return _point[1] < _paths[_point[0]].curve.get_point_count()


func next_point_in_struct(point_addr):
	var struct_index = -1
	for i in range(path_struct.size()):
		var current_in = path_struct[i][0]
		if point_addr == current_in and path_switches[i]:
			struct_index = i + 1
			break
	if struct_index != -1:
		return [struct_index, 1] #----------------------------------
	return null


#Compose list of points, that will create new path
func create_point_sequence():
	var _paths = get_full_paths_list()
	var new_path_points = []
	var finished = false
	var n = 0
	
	var current_point = [0, 0]
	while !finished and n < maximum_segments:
		new_path_points.append(current_point)
		var next_point = [current_point[0], current_point[1] + 1]
		var np = next_point_in_struct(current_point)
		if np:
			next_point = np
			
		if point_exists(next_point):
			current_point = next_point
		else:
			finished = true
		n += 1
	return new_path_points


#Get list of all paths, starting with root path
func get_full_paths_list():
	var _paths = [root_path]
	for p in paths:
		_paths.append(p)
	return _paths


func build_path():
	var train_head_pos = head.global_transform.origin - \
		$Path.global_transform.origin
		
	$Path.curve.clear_points()
	var no_switches_enabled = true
	for s in path_switches:
		if s:
			no_switches_enabled = false
	if (no_switches_enabled):
		for i in root_path.curve.get_point_count():
			$Path.curve.add_point(root_path.curve.get_point_position(i),
				root_path.curve.get_point_in(i),
				root_path.curve.get_point_out(i))
		show_paths()
	else:
		var _paths = get_full_paths_list()
		var new_point_sequence = create_point_sequence()
		var last_path_id = new_point_sequence[0][0]
		for s in new_point_sequence:
			var current_curve = _paths[s[0]].curve
			var point_index = s[1]
			var pos_offset = _paths[s[0]].global_transform.origin
			
			if s[0] != last_path_id and s[1] == 1:
				$Path.curve.set_point_out($Path.curve.get_point_count() - 1,\
					current_curve.get_point_out(point_index - 1))
					
			$Path.curve.add_point(\
				current_curve.get_point_position(point_index) + pos_offset,
				current_curve.get_point_in(point_index),
				current_curve.get_point_out(point_index))
			last_path_id = s[0]
	show_paths()
	train_head_offset = $Path.curve.get_closest_offset(train_head_pos)

func set_switch_value(switch_id:int, switch_value:bool):
	if switch_id < path_switches.size():
		path_switches[switch_id] = switch_value
	build_path()

#                                                   
# 88888  8   88888  88888  88888  8      88888      
# 8   8  8   8   8  8   8  8   8  8      8   8      
# 888    88  88     88  8  88888  88     888        
#    88  88  88 "8  88  8  88  8  88        88      
# 88888  88  88888  88  8  88  8  88888  88888      
# 

func _on_Switch_pressed(switch_id):
	set_switch_value(0, $Control/Button.pressed)

	
func _on_VSlider_value_changed(value):
	speed = $Control/VSlider.value


func _on_VSlider_changed():
	speed = $Control/VSlider.value



