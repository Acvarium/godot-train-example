extends Spatial
var point_obj = preload("res://pointt.tscn")
var pointt_green_obj = preload("res://pointt_green.tscn")

var speed = 1
onready var w_offset = 0.6
var to_switch_path = false

var extra_on = false
onready var train_root_offset = $Path/PathFollow.offset

# чи можна перемкнути даний вузол в даний момент
func path_can_be_switched():
	var head_on_sector = get_head_sector()
	if !extra_on:
		if head_on_sector > 1 and head_on_sector < 3:
			return true
	elif head_on_sector == 1:
		return true
	return false
	

func _ready():
	print($Path.curve.get_baked_length())
	build_path()


func get_head_sector():
	var ss = ""
	var _points = []
	for i in range(1, $Path.curve.get_point_count() - 1):
		_points.append($Path.curve.get_closest_offset(\
			$Path.curve.get_point_position(i)))
	_points.append($Path.curve.get_baked_length())
	ss += str("%.3f" % ($Path/PathFollow.offset))
	var head_on_sector = 0
	var current_head_offset = $Path/PathFollow.offset
	for i in range(_points.size()):
		if  current_head_offset < _points[i]:
			head_on_sector = i
			break
	ss += "   " + str(head_on_sector)
	$Control/Label.text = ss
	return head_on_sector


func _physics_process(delta):
	if to_switch_path:
		if path_can_be_switched():
			switch_path()
	train_root_offset += speed * delta
	$Path/PathFollow.offset = train_root_offset
	$Path/PathFollow2.offset = train_root_offset - w_offset
	$Path/PathFollow3.offset = train_root_offset - w_offset*2
	if get_head_sector() == 0 and !to_switch_path:
		to_switch_path = true


func _on_VSlider_changed():
	speed = $Control/VSlider.value


func _on_VSlider_value_changed(value):
	speed = $Control/VSlider.value


func show_path(_path, _inst, to_clear = true):
	if(to_clear):
		for p in $Points.get_children():
			p.free()
	var step_len = 0.1
	for i in range(int(_path.curve.get_baked_length() / step_len)):
		var point_inst = _inst.instance()
		$Points.add_child(point_inst)
		point_inst.global_transform.origin = _path.curve.interpolate_baked(i * step_len) +\
			_path.global_transform.origin

func build_path(switches = []):
	var train_root_pos = $Path/PathFollow/Model.global_transform.origin - \
		$Path.global_transform.origin
	
	$Path.curve.clear_points()
	
	if (switches.size() == 0):
		for i in $PathRoot/Path.curve.get_point_count():
			$Path.curve.add_point($PathRoot/Path.curve.get_point_position(i),
				$PathRoot/Path.curve.get_point_in(i),
				$PathRoot/Path.curve.get_point_out(i))
		show_path($Path, point_obj)
		show_path($PathSwitch1/Path, pointt_green_obj, false)
	elif switches[0]:
		var switch_in_index = 4
		var switch_out_index = 1
		var _offset = $PathSwitch1/Path.global_transform.origin
		
		
		for i in range(switch_out_index, switch_in_index):
			$Path.curve.add_point($PathRoot/Path.curve.get_point_position(i),
				$PathRoot/Path.curve.get_point_in(i),
				$PathRoot/Path.curve.get_point_out(i))
		$Path.curve.set_point_out($Path.curve.get_point_count() - 1,\
			$PathSwitch1/Path.curve.get_point_out(0))
		for i in range($PathSwitch1/Path.curve.get_point_count() - 1):
			$Path.curve.add_point($PathSwitch1/Path.curve.get_point_position(i + 1) + _offset,
				$PathSwitch1/Path.curve.get_point_in(i + 1) ,
				$PathSwitch1/Path.curve.get_point_out(i + 1))
#		var start_segment_index = $PathRoot/Path.curve.get_closest_point(\
#			$PathSwitch1/In.global_transform.origin - $PathRoot/Path.global_transform.origin)
		show_path($Path, point_obj)
	
	train_root_offset = $Path.curve.get_closest_offset(train_root_pos)

func switch_path():
	to_switch_path = false
	extra_on = !extra_on
	if (extra_on):
		build_path([true])
	else:
		build_path()

func _on_Button_pressed():
	to_switch_path = true
